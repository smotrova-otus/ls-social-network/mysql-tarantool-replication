GOPATH ?= $(HOME)/go
BIN_DIR = $(GOPATH)/bin
PACKR = $(BIN_DIR)/packr2
CMD_DIR = ./cmd/socialnetwork
BRANCH_NAME = $(shell git rev-parse --abbrev-ref HEAD)


IMAGE_NAME := smotrova-otus/ls-social-network/mysql-tarantool-replication
DOCKER_REGISTRY := registry.gitlab.com

.PHONY: docker-build
docker-build:
	docker build -t ${DOCKER_REGISTRY}/${IMAGE_NAME}:${BRANCH_NAME} .

.PHONY: docker-push
docker-push:
	docker push ${DOCKER_REGISTRY}/${IMAGE_NAME}:${BRANCH_NAME}
